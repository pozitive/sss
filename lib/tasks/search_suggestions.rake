namespace :search_suggestions do
  desc "Generate search suggestions from products"
  task :index => :environment do
    SearchSuggestion.index_seaport
  end

  task :fill_seaports => :environment do
    SearchSuggestion.fill_redis_seaports
  end
end
