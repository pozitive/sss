namespace :tariffs do
  desc "Fill in tariffs in redis"
  task :fesco_ints => :environment do
    FescoInt.index_tariffs
  end

end
