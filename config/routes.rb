Rails.application.routes.draw do

  resources :rates
  resources :containers
  resources :maersk_tariffs
  resources :regions
  resources :networks
  resources :seaports

  resources :companies do
    resources :lines, shallow: true do
      resources :points, shallow: true
    end
  end

  resources :lines do
    resources :fesco_ints
  end

  root 'seaports#index'


  # API
  namespace :api do
    namespace :v1 do
      namespace :tariffs do
        resources :pols, only: [:index, :show] do
          resources :pods, only: [:index, :show] do
            resources :containers, only: [:index, :show]
          end
        end
      end
      # get 'tariffs/:company', to: 'tariffs#show'
      # get 'tariffs/:company/pols', to: 'pol_tariffs#index'
      # get 'tariffs/:company/pols/:code', to: 'fesco_pol_pod_tariffs#index'
      # get 'tariffs/:company/pols/:code/pods', to: 'fesco_pol_pod_tariffs#index'
      # get 'tariffs/:company/pols/:code/pods/:code', to: 'fesco_pol_pod_tariffs#index'
      # get 'tariffs/:company/pols/:code/pods/:code/containers', to: 'fesco_pol_pod_tariffs#index'
      # get 'tariffs/:company/pols/:code/pods/:code/containers/:code', to: 'fesco_pol_pod_tariffs#index'
      resources :seaports, only: [:index, :show]
      resources :search_suggestions, only: [:index]
    end
  end


  # resources :fesco_ints do
  #   resources :fesco_int_rates
  # end
  resources :tariffs
end
