class CreateFescoIntTariffs < ActiveRecord::Migration
  def change
    create_table :fesco_int_tariffs do |t|
      t.references :fesco_int, index: true, foreign_key: true
      t.string :status
      t.string :owner
      t.text :containers, array: true, default: []
      t.decimal :freight
      t.decimal :baf

      t.timestamps null: false
    end
  end
end
