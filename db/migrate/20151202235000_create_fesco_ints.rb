class CreateFescoInts < ActiveRecord::Migration
  def change
    create_table :fesco_ints do |t|
      t.references :line, index: true, foreign_key: true
      t.text :pols, array: true, default: []
      t.text :pods, array: true, default: []

      t.timestamps null: false
    end
  end
end
