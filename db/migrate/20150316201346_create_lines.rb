class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.string :name
      t.string :code
      t.text :route
      t.text :map
      t.text :general
      t.text :content
      t.references :company, index: true
    end
  end
end
