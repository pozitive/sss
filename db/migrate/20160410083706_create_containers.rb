class CreateContainers < ActiveRecord::Migration
  def change
    create_table :containers do |t|
      t.string :code
      t.string :name
      t.string :category
      t.text :description

      t.timestamps null: false
    end
  end
end
