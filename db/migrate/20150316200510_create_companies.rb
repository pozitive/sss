class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :code
      t.string :web
      t.text :content
      t.string :logo
    end
  end
end
