class CreateMaerskTariffs < ActiveRecord::Migration
  def change
    create_table :maersk_tariffs do |t|
      t.string :code
      t.string :currency

      t.timestamps null: false
    end
  end
end
