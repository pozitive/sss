class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.references :maersk_tariff, index: true, foreign_key: true
      t.references :container, index: true, foreign_key: true
      t.decimal :sbf

      t.timestamps null: false
    end
  end
end
