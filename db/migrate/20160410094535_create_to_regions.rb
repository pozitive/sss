class CreateToRegions < ActiveRecord::Migration
  def change
    create_table :to_regions do |t|
      t.references :maersk_tariff, index: true, foreign_key: true
      t.references :region, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
