class CreateFromRegions < ActiveRecord::Migration
  def change
    create_table :from_regions do |t|
      t.references :maersk_tariff, index: true, foreign_key: true
      t.references :region, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
