class AddCodeIndexToSeaports < ActiveRecord::Migration
  def change
    add_index :seaports, :code
    add_index :seaports, :name
  end
end
