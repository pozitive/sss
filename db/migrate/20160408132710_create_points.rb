class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.integer :number
      t.string :seaport
      t.string :eta
      t.string :ets
      t.boolean :discharge
      t.boolean :load
      t.integer :transit_time
      t.references :line, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
