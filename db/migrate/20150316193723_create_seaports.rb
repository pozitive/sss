class CreateSeaports < ActiveRecord::Migration
  def change
    create_table :seaports do |t|
      t.string :name
      t.string :country
      t.string :code
      t.string :utc
      t.string :phone
      t.string :web
      t.text :content
      t.float :latitude
      t.float :longitude
    end
  end
end
