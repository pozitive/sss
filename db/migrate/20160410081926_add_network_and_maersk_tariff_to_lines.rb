class AddNetworkAndMaerskTariffToLines < ActiveRecord::Migration
  def change
    add_reference :lines, :network, index: true, foreign_key: true
    add_reference :lines, :maersk_tariff, index: true, foreign_key: true
  end
end
