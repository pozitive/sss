# coding: utf-8
require 'csv'

def ports_csv(file = 'db/seeds/seaports.csv')
  CSV.foreach(file, col_sep: ';',
              headers: true, header_converters: :symbol) do |csv_row|
    seaport = csv_row.to_h
    seaport[:code] = seaport[:code].sub(/\s/, '')
    seaport[:latitude], seaport[:longitude] = convert! seaport[:loc]
    seaport.delete(:loc)
    Seaport.create seaport
  end
end

def convert!(loc)
  # TODO: shoud refactor
  lat, long = loc.split('/')
  tlong = long.match(/\d*°\s*\d*'/).to_s
  tlong.sub!(/°\s*/, '.').sub!("'", '') if tlong.present?
  if long =~ /E/
    long = + tlong.to_f
  elsif long =~ /W/
    long = - tlong.to_f
  end

  tlat = lat.match(/\d*°\s*\d*'/).to_s
  tlat.sub!(/°\s*/, '.').sub!("'", '') if tlat.present?
  if lat =~ /N/
    lat = + tlat.to_f
  elsif lat =~ /S/
    lat = - tlat.to_f
  end
  [lat, long]
end

# Seaport.delete_all
# ports_csv

regions = [%w(OCE Oceania),
 %w(Australia AUS),
 %w(GCA China),
 ['NEA', 'North East Asia'],
 ['FEA', 'Far East Asia'],
 ['USWC', 'West Coast North America'],
 ['USEC', 'East Coast North America']]

regions.each do |r|
  Region.create(code: r[0], name: r[1])
end

containers = [['20DV', "DRY FREIGHT CONTAINERS - 20′DV – STANDARD"],
 ['40DV',"DRY FREIGHT CONTAINERS - 40′DV – STANDARD"],
 ['40HC',"DRY FREIGHT 40 FOOT HIGH CONTAINERS HIGH CUBE- 40′HC"],
 ['45HC',"DRY FREIGHT CONTAINER - 45′HC – HIGH CUBE – HIGHER CAPACITY"],
 ['20REEF',"20 FOOT REFRIGERATED CONTAINERS 20′RF"],
 ['40REEF',"40 FOOT STANDARD REFRIGERATED CONTAINER 40′RF"]]

containers.each do |r|
  Container.create(code: r[0], name: r[1])
end

maersk_content =
<<-MARKDOWN
Maersk Line is a division of the A.P. Moller – Maersk Group, by which we share the same values and business principles.

**Strong Environmental Performance**

Through industry leading environmental performance and a focus on increasing the efficiency of our vessel operations Maersk Line will seek to maintain our CO2 advantage in the industry. Priority issues include energy efficiency of charter vessels, innovative ship design, and innovation in container design and innovation.

**A Responsible Business Partner**

Maersk Line will exercise due care to protect our name and our values. Priority issues include responsible procurement, respect for human and labour rights, and protection of the marine environment.

**The Preferred Choice for Customers**

Maersk Line will work to enable transparency and choice for our customers, supporting their efforts to create more sustainable supply chains and partnering on leadership initiatives to create joint value for sustainable profitable growth.
MARKDOWN

companies =
  [
    ['Maersk Line',
     'Maersk',
     'http://www.maerskline.com',
     maersk_content,
     "http://www.maerskline.com/~/media/maersk-line/brand-assets/MaerskLine-logo.png"]
  ]

companies.each do |c|
  company = Company.find_by(name: c[0], code: c[1], web: c[2], content: c[3], logo: c[4])
  company.delete if company
  Company.create(name: c[0], code: c[1], web: c[2], content: c[3], logo: c[4])
end

lines = [
  ['Alaska Eastbound',
   'TP',
   <<-MARKDOWN 
   ### Service Highlights\n
   • The only service dedicated solely to the Alaska import market\n
   • Fastest transit time from North Asia to Dutch Harbour
   MARKDOWN
  ]
]

lines.each do |l|
  line = Line.find_by(name: l[0], code: l[1], content: l[2], company_id: Company.find_by(code: 'Maersk').id)
  if line
    line.rates.delete_all 
    line.delete
  end
  Line.create(name: l[0], code: l[1], content: l[2], company_id: Company.find_by(code: 'Maersk').id)
end

yokohama      = 'JPYOK'
hakata        = 'JPHKT'
busan         = 'KRPUS'
dalian        = 'CNDLC'
qingdao       = 'CNTAO'
dutch_harbour = 'USDUT'

tp = Line.find_by(code: 'TP', company_id: Company.find_by(code: 'Maersk').id)
tp.points.create(seaport: yokohama, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: true, discharge: false, transit_time: 15, number: 1)
tp.points.create(seaport: hakata, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: true, discharge: false, transit_time: 13, number: 2)
tp.points.create(seaport: busan, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: true, discharge: false, transit_time: 12, number: 3)
tp.points.create(seaport: dalian, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: true, discharge: false, transit_time: 10, number: 4)
tp.points.create(seaport: qingdao, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: true, discharge: false, transit_time: 9, number: 5)
tp.points.create(seaport: dutch_harbour, line_id: tp,
                   eta: 'SUN', ets: 'SUN', load: false, discharge: true, transit_time: 0, number: 6)
