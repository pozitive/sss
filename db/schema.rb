# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160410094535) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "web"
    t.text   "content"
    t.string "logo"
  end

  create_table "containers", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.string   "category"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "fesco_int_tariffs", force: :cascade do |t|
    t.integer  "fesco_int_id"
    t.string   "status"
    t.string   "owner"
    t.text     "containers",   default: [],              array: true
    t.decimal  "freight"
    t.decimal  "baf"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "fesco_int_tariffs", ["fesco_int_id"], name: "index_fesco_int_tariffs_on_fesco_int_id", using: :btree

  create_table "fesco_ints", force: :cascade do |t|
    t.integer  "line_id"
    t.text     "pols",       default: [],              array: true
    t.text     "pods",       default: [],              array: true
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "fesco_ints", ["line_id"], name: "index_fesco_ints_on_line_id", using: :btree

  create_table "from_regions", force: :cascade do |t|
    t.integer  "maersk_tariff_id"
    t.integer  "region_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "from_regions", ["maersk_tariff_id"], name: "index_from_regions_on_maersk_tariff_id", using: :btree
  add_index "from_regions", ["region_id"], name: "index_from_regions_on_region_id", using: :btree

  create_table "lines", force: :cascade do |t|
    t.string  "name"
    t.string  "code"
    t.text    "route"
    t.text    "map"
    t.text    "general"
    t.text    "content"
    t.integer "company_id"
    t.integer "network_id"
    t.integer "maersk_tariff_id"
  end

  add_index "lines", ["company_id"], name: "index_lines_on_company_id", using: :btree
  add_index "lines", ["maersk_tariff_id"], name: "index_lines_on_maersk_tariff_id", using: :btree
  add_index "lines", ["network_id"], name: "index_lines_on_network_id", using: :btree

  create_table "maersk_tariffs", force: :cascade do |t|
    t.string   "code"
    t.string   "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "networks", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "points", force: :cascade do |t|
    t.integer  "number"
    t.string   "seaport"
    t.string   "eta"
    t.string   "ets"
    t.boolean  "discharge"
    t.boolean  "load"
    t.integer  "transit_time"
    t.integer  "line_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "points", ["line_id"], name: "index_points_on_line_id", using: :btree

  create_table "rates", force: :cascade do |t|
    t.integer  "maersk_tariff_id"
    t.integer  "container_id"
    t.decimal  "sbf"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "rates", ["container_id"], name: "index_rates_on_container_id", using: :btree
  add_index "rates", ["maersk_tariff_id"], name: "index_rates_on_maersk_tariff_id", using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "seaports", force: :cascade do |t|
    t.string "name"
    t.string "country"
    t.string "code"
    t.string "utc"
    t.string "phone"
    t.string "web"
    t.text   "content"
    t.float  "latitude"
    t.float  "longitude"
  end

  add_index "seaports", ["code"], name: "index_seaports_on_code", using: :btree
  add_index "seaports", ["name"], name: "index_seaports_on_name", using: :btree

  create_table "to_regions", force: :cascade do |t|
    t.integer  "maersk_tariff_id"
    t.integer  "region_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "to_regions", ["maersk_tariff_id"], name: "index_to_regions_on_maersk_tariff_id", using: :btree
  add_index "to_regions", ["region_id"], name: "index_to_regions_on_region_id", using: :btree

  add_foreign_key "fesco_int_tariffs", "fesco_ints"
  add_foreign_key "fesco_ints", "lines"
  add_foreign_key "from_regions", "maersk_tariffs"
  add_foreign_key "from_regions", "regions"
  add_foreign_key "lines", "maersk_tariffs"
  add_foreign_key "lines", "networks"
  add_foreign_key "points", "lines"
  add_foreign_key "rates", "containers"
  add_foreign_key "rates", "maersk_tariffs"
  add_foreign_key "to_regions", "maersk_tariffs"
  add_foreign_key "to_regions", "regions"
end
