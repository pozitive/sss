require "rails_helper"

RSpec.describe MaerskTariffsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/maersk_tariffs").to route_to("maersk_tariffs#index")
    end

    it "routes to #new" do
      expect(:get => "/maersk_tariffs/new").to route_to("maersk_tariffs#new")
    end

    it "routes to #show" do
      expect(:get => "/maersk_tariffs/1").to route_to("maersk_tariffs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/maersk_tariffs/1/edit").to route_to("maersk_tariffs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/maersk_tariffs").to route_to("maersk_tariffs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/maersk_tariffs/1").to route_to("maersk_tariffs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/maersk_tariffs/1").to route_to("maersk_tariffs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/maersk_tariffs/1").to route_to("maersk_tariffs#destroy", :id => "1")
    end

  end
end
