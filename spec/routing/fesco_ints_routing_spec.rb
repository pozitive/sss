require "rails_helper"

RSpec.describe FescoIntsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fesco_ints").to route_to("fesco_ints#index")
    end

    it "routes to #new" do
      expect(:get => "/fesco_ints/new").to route_to("fesco_ints#new")
    end

    it "routes to #show" do
      expect(:get => "/fesco_ints/1").to route_to("fesco_ints#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fesco_ints/1/edit").to route_to("fesco_ints#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fesco_ints").to route_to("fesco_ints#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/fesco_ints/1").to route_to("fesco_ints#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/fesco_ints/1").to route_to("fesco_ints#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fesco_ints/1").to route_to("fesco_ints#destroy", :id => "1")
    end

  end
end
