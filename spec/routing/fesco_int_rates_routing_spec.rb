require "rails_helper"

RSpec.describe FescoIntRatesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fesco_int_rates").to route_to("fesco_int_rates#index")
    end

    it "routes to #new" do
      expect(:get => "/fesco_int_rates/new").to route_to("fesco_int_rates#new")
    end

    it "routes to #show" do
      expect(:get => "/fesco_int_rates/1").to route_to("fesco_int_rates#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fesco_int_rates/1/edit").to route_to("fesco_int_rates#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fesco_int_rates").to route_to("fesco_int_rates#create")
    end

    it "routes to #update" do
      expect(:put => "/fesco_int_rates/1").to route_to("fesco_int_rates#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fesco_int_rates/1").to route_to("fesco_int_rates#destroy", :id => "1")
    end

  end
end
