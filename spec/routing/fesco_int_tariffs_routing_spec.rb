require "rails_helper"

RSpec.describe FescoIntTariffsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fesco_int_tariffs").to route_to("fesco_int_tariffs#index")
    end

    it "routes to #new" do
      expect(:get => "/fesco_int_tariffs/new").to route_to("fesco_int_tariffs#new")
    end

    it "routes to #show" do
      expect(:get => "/fesco_int_tariffs/1").to route_to("fesco_int_tariffs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fesco_int_tariffs/1/edit").to route_to("fesco_int_tariffs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fesco_int_tariffs").to route_to("fesco_int_tariffs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/fesco_int_tariffs/1").to route_to("fesco_int_tariffs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/fesco_int_tariffs/1").to route_to("fesco_int_tariffs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fesco_int_tariffs/1").to route_to("fesco_int_tariffs#destroy", :id => "1")
    end

  end
end
