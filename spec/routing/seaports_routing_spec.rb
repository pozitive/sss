require "rails_helper"

RSpec.describe SeaportsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/seaports").to route_to("seaports#index")
    end

    it "routes to #new" do
      expect(:get => "/seaports/new").to route_to("seaports#new")
    end

    it "routes to #show" do
      expect(:get => "/seaports/1").to route_to("seaports#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/seaports/1/edit").to route_to("seaports#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/seaports").to route_to("seaports#create")
    end

    it "routes to #update" do
      expect(:put => "/seaports/1").to route_to("seaports#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/seaports/1").to route_to("seaports#destroy", :id => "1")
    end

  end
end
