require 'rails_helper'

RSpec.describe "seaports/show", :type => :view do
  before(:each) do
    @seaport = assign(:seaport, Seaport.create!(
      :name => "Name",
      :country => "Country",
      :code => "Code",
      :loc => "",
      :utc => "Utc",
      :phone => "Phone",
      :web => "Web"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Utc/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Web/)
  end
end
