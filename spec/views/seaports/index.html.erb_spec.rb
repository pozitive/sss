require 'rails_helper'

RSpec.describe "seaports/index", :type => :view do
  before(:each) do
    assign(:seaports, [
      Seaport.create!(
        :name => "Name",
        :country => "Country",
        :code => "Code",
        :loc => "",
        :utc => "Utc",
        :phone => "Phone",
        :web => "Web"
      ),
      Seaport.create!(
        :name => "Name",
        :country => "Country",
        :code => "Code",
        :loc => "",
        :utc => "Utc",
        :phone => "Phone",
        :web => "Web"
      )
    ])
  end

  it "renders a list of seaports" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Utc".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Web".to_s, :count => 2
  end
end
