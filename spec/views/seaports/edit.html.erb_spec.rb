require 'rails_helper'

RSpec.describe "seaports/edit", :type => :view do
  before(:each) do
    @seaport = assign(:seaport, Seaport.create!(
      :name => "MyString",
      :country => "MyString",
      :code => "MyString",
      :loc => "",
      :utc => "MyString",
      :phone => "MyString",
      :web => "MyString"
    ))
  end

  it "renders the edit seaport form" do
    render

    assert_select "form[action=?][method=?]", seaport_path(@seaport), "post" do

      assert_select "input#seaport_name[name=?]", "seaport[name]"

      assert_select "input#seaport_country[name=?]", "seaport[country]"

      assert_select "input#seaport_code[name=?]", "seaport[code]"

      assert_select "input#seaport_loc[name=?]", "seaport[loc]"

      assert_select "input#seaport_utc[name=?]", "seaport[utc]"

      assert_select "input#seaport_phone[name=?]", "seaport[phone]"

      assert_select "input#seaport_web[name=?]", "seaport[web]"
    end
  end
end
