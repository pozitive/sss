require 'rails_helper'

RSpec.describe "networks/new", type: :view do
  before(:each) do
    assign(:network, Network.new(
      :name => "MyString",
      :code => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new network form" do
    render

    assert_select "form[action=?][method=?]", networks_path, "post" do

      assert_select "input#network_name[name=?]", "network[name]"

      assert_select "input#network_code[name=?]", "network[code]"

      assert_select "textarea#network_description[name=?]", "network[description]"
    end
  end
end
