require 'rails_helper'

RSpec.describe "networks/index", type: :view do
  before(:each) do
    assign(:networks, [
      Network.create!(
        :name => "Name",
        :code => "Code",
        :description => "MyText"
      ),
      Network.create!(
        :name => "Name",
        :code => "Code",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of networks" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
