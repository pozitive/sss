require 'rails_helper'

RSpec.describe "companies/edit", :type => :view do
  before(:each) do
    @company = assign(:company, Company.create!(
      :name => "MyString",
      :code => "MyString",
      :web => "MyString",
      :content => "MyString",
      :logo => ""
    ))
  end

  it "renders the edit company form" do
    render

    assert_select "form[action=?][method=?]", company_path(@company), "post" do

      assert_select "input#company_name[name=?]", "company[name]"

      assert_select "input#company_code[name=?]", "company[code]"

      assert_select "input#company_web[name=?]", "company[web]"

      assert_select "input#company_content[name=?]", "company[content]"

      assert_select "input#company_logo[name=?]", "company[logo]"
    end
  end
end
