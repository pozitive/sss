require 'rails_helper'

RSpec.describe "companies/show", :type => :view do
  before(:each) do
    @company = assign(:company, Company.create!(
      :name => "Name",
      :code => "Code",
      :web => "Web",
      :content => "Content",
      :logo => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/Web/)
    expect(rendered).to match(/Content/)
    expect(rendered).to match(//)
  end
end
