require 'rails_helper'

RSpec.describe "tariffs/edit", :type => :view do
  before(:each) do
    @tariff = assign(:tariff, Tariff.create!())
  end

  it "renders the edit tariff form" do
    render

    assert_select "form[action=?][method=?]", tariff_path(@tariff), "post" do
    end
  end
end
