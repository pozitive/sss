require 'rails_helper'

RSpec.describe "tariffs/new", :type => :view do
  before(:each) do
    assign(:tariff, Tariff.new())
  end

  it "renders new tariff form" do
    render

    assert_select "form[action=?][method=?]", tariffs_path, "post" do
    end
  end
end
