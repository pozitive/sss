require 'rails_helper'

RSpec.describe "rates/new", type: :view do
  before(:each) do
    assign(:rate, Rate.new(
      :maersk_tariff => nil,
      :container => nil,
      :sbf => "9.99"
    ))
  end

  it "renders new rate form" do
    render

    assert_select "form[action=?][method=?]", rates_path, "post" do

      assert_select "input#rate_maersk_tariff_id[name=?]", "rate[maersk_tariff_id]"

      assert_select "input#rate_container_id[name=?]", "rate[container_id]"

      assert_select "input#rate_sbf[name=?]", "rate[sbf]"
    end
  end
end
