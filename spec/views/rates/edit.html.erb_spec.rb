require 'rails_helper'

RSpec.describe "rates/edit", type: :view do
  before(:each) do
    @rate = assign(:rate, Rate.create!(
      :maersk_tariff => nil,
      :container => nil,
      :sbf => "9.99"
    ))
  end

  it "renders the edit rate form" do
    render

    assert_select "form[action=?][method=?]", rate_path(@rate), "post" do

      assert_select "input#rate_maersk_tariff_id[name=?]", "rate[maersk_tariff_id]"

      assert_select "input#rate_container_id[name=?]", "rate[container_id]"

      assert_select "input#rate_sbf[name=?]", "rate[sbf]"
    end
  end
end
