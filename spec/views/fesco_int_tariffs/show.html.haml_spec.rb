require 'rails_helper'

RSpec.describe "fesco_int_tariffs/show", type: :view do
  before(:each) do
    @fesco_int_tariff = assign(:fesco_int_tariff, FescoIntTariff.create!(
      :fesco_int => nil,
      :status => "Status",
      :owner => "Owner",
      :containers => "MyText",
      :freight => "9.99",
      :baf => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Owner/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
  end
end
