require 'rails_helper'

RSpec.describe "fesco_int_tariffs/edit", type: :view do
  before(:each) do
    @fesco_int_tariff = assign(:fesco_int_tariff, FescoIntTariff.create!(
      :fesco_int => nil,
      :status => "MyString",
      :owner => "MyString",
      :containers => "MyText",
      :freight => "9.99",
      :baf => "9.99"
    ))
  end

  it "renders the edit fesco_int_tariff form" do
    render

    assert_select "form[action=?][method=?]", fesco_int_tariff_path(@fesco_int_tariff), "post" do

      assert_select "input#fesco_int_tariff_fesco_int_id[name=?]", "fesco_int_tariff[fesco_int_id]"

      assert_select "input#fesco_int_tariff_status[name=?]", "fesco_int_tariff[status]"

      assert_select "input#fesco_int_tariff_owner[name=?]", "fesco_int_tariff[owner]"

      assert_select "textarea#fesco_int_tariff_containers[name=?]", "fesco_int_tariff[containers]"

      assert_select "input#fesco_int_tariff_freight[name=?]", "fesco_int_tariff[freight]"

      assert_select "input#fesco_int_tariff_baf[name=?]", "fesco_int_tariff[baf]"
    end
  end
end
