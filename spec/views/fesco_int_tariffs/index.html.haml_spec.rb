require 'rails_helper'

RSpec.describe "fesco_int_tariffs/index", type: :view do
  before(:each) do
    assign(:fesco_int_tariffs, [
      FescoIntTariff.create!(
        :fesco_int => nil,
        :status => "Status",
        :owner => "Owner",
        :containers => "MyText",
        :freight => "9.99",
        :baf => "9.99"
      ),
      FescoIntTariff.create!(
        :fesco_int => nil,
        :status => "Status",
        :owner => "Owner",
        :containers => "MyText",
        :freight => "9.99",
        :baf => "9.99"
      )
    ])
  end

  it "renders a list of fesco_int_tariffs" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Owner".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
