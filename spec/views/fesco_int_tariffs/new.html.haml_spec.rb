require 'rails_helper'

RSpec.describe "fesco_int_tariffs/new", type: :view do
  before(:each) do
    assign(:fesco_int_tariff, FescoIntTariff.new(
      :fesco_int => nil,
      :status => "MyString",
      :owner => "MyString",
      :containers => "MyText",
      :freight => "9.99",
      :baf => "9.99"
    ))
  end

  it "renders new fesco_int_tariff form" do
    render

    assert_select "form[action=?][method=?]", fesco_int_tariffs_path, "post" do

      assert_select "input#fesco_int_tariff_fesco_int_id[name=?]", "fesco_int_tariff[fesco_int_id]"

      assert_select "input#fesco_int_tariff_status[name=?]", "fesco_int_tariff[status]"

      assert_select "input#fesco_int_tariff_owner[name=?]", "fesco_int_tariff[owner]"

      assert_select "textarea#fesco_int_tariff_containers[name=?]", "fesco_int_tariff[containers]"

      assert_select "input#fesco_int_tariff_freight[name=?]", "fesco_int_tariff[freight]"

      assert_select "input#fesco_int_tariff_baf[name=?]", "fesco_int_tariff[baf]"
    end
  end
end
