require 'rails_helper'

RSpec.describe "fesco_ints/index", type: :view do
  before(:each) do
    assign(:fesco_ints, [
      FescoInt.create!(
        :line => nil,
        :pols => "MyText",
        :pods => "MyText",
        :status => "Status",
        :owner => "Owner",
        :containers => "MyText",
        :freight => 1,
        :baf => 2
      ),
      FescoInt.create!(
        :line => nil,
        :pols => "MyText",
        :pods => "MyText",
        :status => "Status",
        :owner => "Owner",
        :containers => "MyText",
        :freight => 1,
        :baf => 2
      )
    ])
  end

  it "renders a list of fesco_ints" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Owner".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
