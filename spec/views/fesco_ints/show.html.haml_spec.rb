require 'rails_helper'

RSpec.describe "fesco_ints/show", type: :view do
  before(:each) do
    @fesco_int = assign(:fesco_int, FescoInt.create!(
      :line => nil,
      :pols => "MyText",
      :pods => "MyText",
      :status => "Status",
      :owner => "Owner",
      :containers => "MyText",
      :freight => 1,
      :baf => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Owner/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
