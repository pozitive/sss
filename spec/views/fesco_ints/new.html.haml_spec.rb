require 'rails_helper'

RSpec.describe "fesco_ints/new", type: :view do
  before(:each) do
    assign(:fesco_int, FescoInt.new(
      :line => nil,
      :pols => "MyText",
      :pods => "MyText",
      :status => "MyString",
      :owner => "MyString",
      :containers => "MyText",
      :freight => 1,
      :baf => 1
    ))
  end

  it "renders new fesco_int form" do
    render

    assert_select "form[action=?][method=?]", fesco_ints_path, "post" do

      assert_select "input#fesco_int_line_id[name=?]", "fesco_int[line_id]"

      assert_select "textarea#fesco_int_pols[name=?]", "fesco_int[pols]"

      assert_select "textarea#fesco_int_pods[name=?]", "fesco_int[pods]"

      assert_select "input#fesco_int_status[name=?]", "fesco_int[status]"

      assert_select "input#fesco_int_owner[name=?]", "fesco_int[owner]"

      assert_select "textarea#fesco_int_containers[name=?]", "fesco_int[containers]"

      assert_select "input#fesco_int_freight[name=?]", "fesco_int[freight]"

      assert_select "input#fesco_int_baf[name=?]", "fesco_int[baf]"
    end
  end
end
