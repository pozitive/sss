require 'rails_helper'

RSpec.describe "containers/edit", :type => :view do
  before(:each) do
    @container = assign(:container, Container.create!(
      :name => "MyString",
      :code => "MyString"
    ))
  end

  it "renders the edit container form" do
    render

    assert_select "form[action=?][method=?]", container_path(@container), "post" do

      assert_select "input#container_name[name=?]", "container[name]"

      assert_select "input#container_code[name=?]", "container[code]"
    end
  end
end
