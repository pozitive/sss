require 'rails_helper'

RSpec.describe "containers/index", type: :view do
  before(:each) do
    assign(:containers, [
      Container.create!(
        :code => "Code",
        :name => "Name",
        :category => "Category",
        :description => "MyText"
      ),
      Container.create!(
        :code => "Code",
        :name => "Name",
        :category => "Category",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of containers" do
    render
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
