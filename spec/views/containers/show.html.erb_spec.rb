require 'rails_helper'

RSpec.describe "containers/show", type: :view do
  before(:each) do
    @container = assign(:container, Container.create!(
      :code => "Code",
      :name => "Name",
      :category => "Category",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Category/)
    expect(rendered).to match(/MyText/)
  end
end
