require 'rails_helper'

RSpec.describe "containers/show", :type => :view do
  before(:each) do
    @container = assign(:container, Container.create!(
      :name => "Name",
      :code => "Code"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Code/)
  end
end
