require 'rails_helper'

RSpec.describe "containers/new", type: :view do
  before(:each) do
    assign(:container, Container.new(
      :code => "MyString",
      :name => "MyString",
      :category => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new container form" do
    render

    assert_select "form[action=?][method=?]", containers_path, "post" do

      assert_select "input#container_code[name=?]", "container[code]"

      assert_select "input#container_name[name=?]", "container[name]"

      assert_select "input#container_category[name=?]", "container[category]"

      assert_select "textarea#container_description[name=?]", "container[description]"
    end
  end
end
