require 'rails_helper'

RSpec.describe "containers/index", :type => :view do
  before(:each) do
    assign(:containers, [
      Container.create!(
        :name => "Name",
        :code => "Code"
      ),
      Container.create!(
        :name => "Name",
        :code => "Code"
      )
    ])
  end

  it "renders a list of containers" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
  end
end
