require 'rails_helper'

RSpec.describe "fesco_int_rates/show", :type => :view do
  before(:each) do
    @fesco_int_rate = assign(:fesco_int_rate, FescoIntRate.create!(
      :status => "Status",
      :owner => "Owner",
      :freight => "",
      :baf => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Owner/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
