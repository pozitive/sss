require 'rails_helper'

RSpec.describe "fesco_int_rates/index", :type => :view do
  before(:each) do
    assign(:fesco_int_rates, [
      FescoIntRate.create!(
        :status => "Status",
        :owner => "Owner",
        :freight => "",
        :baf => ""
      ),
      FescoIntRate.create!(
        :status => "Status",
        :owner => "Owner",
        :freight => "",
        :baf => ""
      )
    ])
  end

  it "renders a list of fesco_int_rates" do
    render
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Owner".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
