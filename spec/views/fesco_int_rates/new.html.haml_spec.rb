require 'rails_helper'

RSpec.describe "fesco_int_rates/new", :type => :view do
  before(:each) do
    assign(:fesco_int_rate, FescoIntRate.new(
      :status => "MyString",
      :owner => "MyString",
      :freight => "",
      :baf => ""
    ))
  end

  it "renders new fesco_int_rate form" do
    render

    assert_select "form[action=?][method=?]", fesco_int_rates_path, "post" do

      assert_select "input#fesco_int_rate_status[name=?]", "fesco_int_rate[status]"

      assert_select "input#fesco_int_rate_owner[name=?]", "fesco_int_rate[owner]"

      assert_select "input#fesco_int_rate_freight[name=?]", "fesco_int_rate[freight]"

      assert_select "input#fesco_int_rate_baf[name=?]", "fesco_int_rate[baf]"
    end
  end
end
