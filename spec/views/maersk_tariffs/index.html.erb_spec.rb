require 'rails_helper'

RSpec.describe "maersk_tariffs/index", type: :view do
  before(:each) do
    assign(:maersk_tariffs, [
      MaerskTariff.create!(
        :currency => "Currency"
      ),
      MaerskTariff.create!(
        :currency => "Currency"
      )
    ])
  end

  it "renders a list of maersk_tariffs" do
    render
    assert_select "tr>td", :text => "Currency".to_s, :count => 2
  end
end
