require 'rails_helper'

RSpec.describe "maersk_tariffs/edit", type: :view do
  before(:each) do
    @maersk_tariff = assign(:maersk_tariff, MaerskTariff.create!(
      :currency => "MyString"
    ))
  end

  it "renders the edit maersk_tariff form" do
    render

    assert_select "form[action=?][method=?]", maersk_tariff_path(@maersk_tariff), "post" do

      assert_select "input#maersk_tariff_currency[name=?]", "maersk_tariff[currency]"
    end
  end
end
