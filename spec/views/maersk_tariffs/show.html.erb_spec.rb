require 'rails_helper'

RSpec.describe "maersk_tariffs/show", type: :view do
  before(:each) do
    @maersk_tariff = assign(:maersk_tariff, MaerskTariff.create!(
      :currency => "Currency"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Currency/)
  end
end
