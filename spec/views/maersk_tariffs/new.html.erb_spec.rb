require 'rails_helper'

RSpec.describe "maersk_tariffs/new", type: :view do
  before(:each) do
    assign(:maersk_tariff, MaerskTariff.new(
      :currency => "MyString"
    ))
  end

  it "renders new maersk_tariff form" do
    render

    assert_select "form[action=?][method=?]", maersk_tariffs_path, "post" do

      assert_select "input#maersk_tariff_currency[name=?]", "maersk_tariff[currency]"
    end
  end
end
