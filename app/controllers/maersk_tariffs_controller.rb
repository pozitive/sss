class MaerskTariffsController < ApplicationController
  before_action :set_maersk_tariff, only: [:show, :edit, :update, :destroy]

  # GET /maersk_tariffs
  # GET /maersk_tariffs.json
  def index
    @maersk_tariffs = MaerskTariff.all
  end

  # GET /maersk_tariffs/1
  # GET /maersk_tariffs/1.json
  def show
  end

  # GET /maersk_tariffs/new
  def new
    @maersk_tariff = MaerskTariff.new
    fill_default
  end

  # GET /maersk_tariffs/1/edit
  def edit
  end

  # POST /maersk_tariffs
  # POST /maersk_tariffs.json
  def create
    @maersk_tariff = MaerskTariff.new(maersk_tariff_params)

    respond_to do |format|
      if @maersk_tariff.save
        format.html { redirect_to @maersk_tariff, notice: 'Maersk tariff was successfully created.' }
        format.json { render :show, status: :created, location: @maersk_tariff }
      else
        format.html { render :new }
        format.json { render json: @maersk_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /maersk_tariffs/1
  # PATCH/PUT /maersk_tariffs/1.json
  def update
    respond_to do |format|
      if @maersk_tariff.update(maersk_tariff_params)
        format.html { redirect_to @maersk_tariff, notice: 'Maersk tariff was successfully updated.' }
        format.json { render :show, status: :ok, location: @maersk_tariff }
      else
        format.html { render :edit }
        format.json { render json: @maersk_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maersk_tariffs/1
  # DELETE /maersk_tariffs/1.json
  def destroy
    @maersk_tariff.destroy
    respond_to do |format|
      format.html { redirect_to maersk_tariffs_url, notice: 'Maersk tariff was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_maersk_tariff
      @maersk_tariff = MaerskTariff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def maersk_tariff_params
      params.require(:maersk_tariff).permit(:code, :currency, from_region_ids:[], to_region_ids:[], rates_attributes: [:id, :container_id, :sbf, :_destroy ])
    end

    def fill_default
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '20DV').id)
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '40DV').id)
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '40HC').id)
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '45HC').id)
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '20REEF').id)
      @maersk_tariff.rates.new(container_id: Container.find_by(code: '40REEF').id)
    end
end
