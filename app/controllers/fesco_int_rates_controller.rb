class FescoIntRatesController < ApplicationController
  before_action :load_fesco_int
  before_action :set_fesco_int_rate, only: [:show, :edit, :update, :destroy]

  # def index
  #   @fesco_int_rates = @fesco_int.fesco_int_rates
  # end

  # def show
  # end

  def new
    @fesco_int_rate = @fesco_int.fesco_int_rates.new
  end

  def edit
  end

  def create
    @fesco_int_rate = @fesco_int.fesco_int_rates.build(fesco_int_rate_params)
    respond_to do |format|
      if @fesco_int_rate.save
        format.html { redirect_to @fesco_int, notice: 'Fesco int rate was successfully created.' }
        format.json { render :show, status: :created, location: @fesco_int_rate }
      else
        format.html { render :new }
        format.json { render json: @fesco_int.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @fesco_int.fesco_int_rate.update(fesco_int_rate_params)
        format.html { redirect_to @fesco_int, notice: 'Fesco int rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @fesco_int_rate }
      else
        format.html { render :edit }
        format.json { render json: @fesco_int.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @fesco_int_rate.delete
    respond_to do |format|
      format.html { redirect_to @fesco_int, notice: 'Fesco int rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fesco_int_rate
      @fesco_int_rate = @fesco_int.fesco_int_rates.find(params[:id])
    end

    def load_fesco_int
      @fesco_int = FescoInt.find(params[:fesco_int_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fesco_int_rate_params
      params.require(:fesco_int_rate).permit(:status, :owner, :freight, :baf)
    end
end
