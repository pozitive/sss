class SeaportsController < ApplicationController
  layout "application"
  before_action :set_seaport, only: [:show, :edit, :update, :destroy]

  # GET /seaports
  # GET /seaports.json
  def index
    @seaports = Seaport.where("code || name || country ILIKE ?", "%#{params[:q]}%")
  end

  # GET /seaports/1
  # GET /seaports/1.json
  def show
  end

  # GET /seaports/new
  def new
    @seaport = Seaport.new
  end

  # GET /seaports/1/edit
  def edit
  end

  # POST /seaports
  # POST /seaports.json
  def create
    @seaport = Seaport.new(seaport_params)

    respond_to do |format|
      if @seaport.save
        format.html { redirect_to @seaport, notice: 'Seaport was successfully created.' }
        format.json { render :show, status: :created, location: @seaport }
      else
        format.html { render :new }
        format.json { render json: @seaport.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seaports/1
  # PATCH/PUT /seaports/1.json
  def update
    respond_to do |format|
      if @seaport.update(seaport_params)
        format.html { redirect_to @seaport, notice: 'Seaport was successfully updated.' }
        format.json { render :show, status: :ok, location: @seaport }
      else
        format.html { render :edit }
        format.json { render json: @seaport.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seaports/1
  # DELETE /seaports/1.json
  def destroy
    @seaport.destroy
    respond_to do |format|
      format.html { redirect_to seaports_url, notice: 'Seaport was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seaport
      code = params[:id].upcase
      @seaport = Seaport.friendly.find(code)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seaport_params
      params.require(:seaport).permit(:name, :country, :code, :loc, :utc, :phone, :web, :latitude, :longitude)
    end
end
