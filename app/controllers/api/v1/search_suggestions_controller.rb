class Api::V1::SearchSuggestionsController < ApplicationController
  def index
    render json: SearchSuggestion.terms_for(params[:q])
  end
end
