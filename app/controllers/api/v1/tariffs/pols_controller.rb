class Api::V1::Tariffs::PolsController < ApplicationController
  before_action :set_pol, only: [:show]

  def index
    @tariffs = {}
    @tariffs[:tariffs] = FescoIntTariff.get_all_redis.reject { |c| c.empty? }

    render json: @tariffs.re
  end

  def show
    @tariffs = {:tariffs => []}
    
    @tariffs[:tariffs] = FescoIntTariff.get_all_redis([@pol]).reject { |c| c.empty? }

    render json: @tariffs
  end
  
  private

  def set_pol
    @pol = params[:id].upcase
  end
end
