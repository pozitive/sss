class Api::V1::Tariffs::ContainersController < ApplicationController
  before_action :set_pol
  before_action :set_pod
  before_action :set_container, only: [:show]

  def index
    @tariffs = {}

    @tariffs[:tariffs] = FescoIntTariff.get_all_redis([@pol], [@pod]).reject { |c| c.empty? }

    render json: @tariffs
  end

  def show
    @tariffs = {}

    @tariffs[:tariffs] = FescoIntTariff.get_all_redis([@pol],[@pod],[@container]).reject { |c| c.empty? }
    

    render json: @tariffs
  end
  
  private

  def set_pol
    @pol = params[:pol_id].upcase
  end

  def set_pod
    @pod = params[:pod_id].upcase
  end

  def set_container
    @container = params[:id].upcase
  end

end
