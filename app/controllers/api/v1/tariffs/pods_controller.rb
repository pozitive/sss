class Api::V1::Tariffs::PodsController < ApplicationController
  before_action :set_pol
  before_action :set_pod, only: [:show]

  def index
    @tariffs = {}
    @tariffs[:tariffs] = FescoIntTariff.get_all_redis([@pol]).reject { |c| c.empty? }

    render json: @tariffs
  end

  def show
    @tariffs = {:tariffs => []}
    
    # Fesco
    @tariffs[:tariffs] = FescoIntTariff.get_all_redis([@pol],[@pod]).reject { |c| c.empty? }

    # Maersk
    lines = Line.joins(:points).where(points: {seaport: 'JPYOK', load: true}).where.not(maersk_tariff: nil)
    lines = lines.select {|l| l.points.where(seaport: 'USDUT', discharge: true).any? }
    lines.each do |line|
      line.maersk_tariff.rates.each do |rate|
        @tariffs[:tariffs] << {
          company: "Maersk",
          pol: "RUVVO",
          pod: "CNSHA",
          container: rate.container.code,
          sbf: rate.sbf
        }
      end
    end

    render json: @tariffs
  end
  
  private

  def set_pol
    @pol = params[:pol_id].upcase
  end

  def set_pod
    @pod = params[:id].upcase
  end
end
