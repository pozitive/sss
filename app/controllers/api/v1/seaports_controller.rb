class Api::V1::SeaportsController < ApplicationController
  before_action :set_seaport, only: [:show]

  def index
    @seaports = {seaports: [] }
    set_codes.each do |code|
      @seaports[:seaports] << ($redis.hgetall "seaports:#{code}")
    end

    render json: @seaports
  end

  def show
    render json: @seaport
  end

  private

  def set_seaport
    code = params[:id].upcase
    @seaport = Seaport.friendly.find(code)
  end

  def set_codes
    codes = SearchSuggestion.terms_for(params[:q]) 
    if codes.empty?
    then
      codes = ($redis.lrange "seaports:codes", 0, -1)
    end
    return codes
  end

end
