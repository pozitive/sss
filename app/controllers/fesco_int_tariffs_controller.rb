class FescoIntTariffsController < ApplicationController
  before_action :set_fesco_int_tariff, only: [:show, :edit, :update, :destroy]

  # GET /fesco_int_tariffs
  # GET /fesco_int_tariffs.json
  def index
    @fesco_int_tariffs = FescoIntTariff.all
  end

  # GET /fesco_int_tariffs/1
  # GET /fesco_int_tariffs/1.json
  def show
  end

  # GET /fesco_int_tariffs/new
  def new
    @fesco_int_tariff = FescoIntTariff.new
  end

  # GET /fesco_int_tariffs/1/edit
  def edit
  end

  # POST /fesco_int_tariffs
  # POST /fesco_int_tariffs.json
  def create
    @fesco_int_tariff = FescoIntTariff.new(fesco_int_tariff_params)

    respond_to do |format|
      if @fesco_int_tariff.save
        format.html { redirect_to @fesco_int_tariff, notice: 'Fesco int tariff was successfully created.' }
        format.json { render :show, status: :created, location: @fesco_int_tariff }
      else
        format.html { render :new }
        format.json { render json: @fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fesco_int_tariffs/1
  # PATCH/PUT /fesco_int_tariffs/1.json
  def update
    respond_to do |format|
      if @fesco_int_tariff.update(fesco_int_tariff_params)
        format.html { redirect_to @fesco_int_tariff, notice: 'Fesco int tariff was successfully updated.' }
        format.json { render :show, status: :ok, location: @fesco_int_tariff }
      else
        format.html { render :edit }
        format.json { render json: @fesco_int_tariff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fesco_int_tariffs/1
  # DELETE /fesco_int_tariffs/1.json
  def destroy
    @fesco_int_tariff.destroy
    respond_to do |format|
      format.html { redirect_to fesco_int_tariffs_url, notice: 'Fesco int tariff was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fesco_int_tariff
      @fesco_int_tariff = FescoIntTariff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fesco_int_tariff_params
      params.require(:fesco_int_tariff).permit(:fesco_int_id, :status, :owner, :containers, :freight, :baf)
    end
end
