class LinesController < ApplicationController
  before_action :set_company, only: [:create, :new, :index]
  before_action :set_line, only: [:show, :edit, :update, :destroy]

  def index
    @lines = @company.lines
  end

  def show
  end

  def edit
  end

  def new
    @line = @company.lines.new
  end

  def create
    @line = @company.lines.build(line_params)

    respond_to do |format|
      if @line.save
        format.html { redirect_to @line, notice: 'Line was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new, notice: 'Line was not created' }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @line.update(line_params)
        format.html { redirect_to @line, notice: 'Line was successfully updated.' }
        format.json { render :show, status: :ok, location: @line.company }
      else
        format.html { render :edit, notice: 'Line was not updated.' }
        format.json { render json: @line.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @line.destroy
    redirect_to @line.company, notice: 'Line was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:company_id])
    end

    def set_line
      @line = Line.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_params
      params.require(:line).permit(:name, :code, :content, :network, :maersk_tariff_id)
    end

end
