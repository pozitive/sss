class FescoIntsController < ApplicationController
  before_action :set_fesco_int, only: [:show, :edit, :update, :destroy]
  before_action :set_seaports, only: [:new, :edit]
  before_action :set_line

  # GET /fesco_ints
  # GET /fesco_ints.json
  def index
    @fesco_ints = FescoInt.all
  end

  # GET /fesco_ints/1
  # GET /fesco_ints/1.json
  def show
    @pols = @fesco_int.pols.map{ |p| Seaport.find_by(code: p).name }
    @pods = @fesco_int.pods.map{ |p| Seaport.find_by(code: p).name }
  end

  # GET /fesco_ints/new
  def new
    @fesco_int = FescoInt.new
    fill_default
  end

  # GET /fesco_ints/1/edit
  def edit
  end

  # POST /fesco_ints
  # POST /fesco_ints.json
  def create
    @fesco_int = FescoInt.new(fesco_int_params)

    respond_to do |format|
      if @fesco_int.save
        format.html { redirect_to line_fesco_int_path(@line, @fesco_int), notice: 'Fesco int was successfully created.' }
        format.json { render :show, status: :created, location: line_fesco_int_path(@line, @fesco_int) }
      else
        format.html { render :new }
        format.json { render json: @fesco_int.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fesco_ints/1
  # PATCH/PUT /fesco_ints/1.json
  def update
    respond_to do |format|
      if @fesco_int.update(fesco_int_params)
        format.html { redirect_to line_fesco_int_path(@line, @fesco_int), notice: 'Fesco int was successfully updated.' }
        format.json { render :show, status: :ok, location: line_fesco_int_path(@line, @fesco_int) }
      else
        format.html { render :edit }
        format.json { render json: @fesco_int.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fesco_ints/1
  # DELETE /fesco_ints/1.json
  def destroy
    @fesco_int.destroy
    respond_to do |format|
      format.html { redirect_to line_url(@line), notice: 'Fesco int was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_line
      @line = Line.find(params[:line_id])
    end

    def set_fesco_int
      @fesco_int = FescoInt.find(params[:id])
    end

    def set_seaports
      @seaports = Seaport.all.map{|p| [p.name, p.code]}
    end

    def fesco_int_params
      params[:fesco_int][:pols] = params[:fesco_int][:pols].reject{|a| a.empty?}
      params[:fesco_int][:pods] = params[:fesco_int][:pods].reject{|a| a.empty?}
      params.require(:fesco_int).permit(:line_id, :pols => [], :pods => [], :fesco_int_tariffs_attributes => [ :id, :status, :owner, :freight, :baf, :_destroy, :containers => [] ])
    end

    def fill_default
      @fesco_int.fesco_int_tariffs.new(status: "Empty", owner: "SOC", containers: ["20'"])
      @fesco_int.fesco_int_tariffs.new(status: "Empty", owner: "SOC", containers: ["40'", "40'HC"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "COC", containers: ["20'DC"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "COC", containers: ["40'", "40'HC"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "SOC", containers: ["20'DC"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "SOC", containers: ["40'", "40'HC"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "SOC", containers: ["20'RF"])
      @fesco_int.fesco_int_tariffs.new(status: "Full", owner: "SOC", containers: ["40'RF"])
    end
end
