class MaerskTariff < ActiveRecord::Base
  has_many :lines
  has_many :rates, dependent: :destroy
  has_many :trades

  has_many :froms, :class_name => "FromRegion", dependent: :destroy
  has_many :tos, :class_name => "ToRegion", dependent: :destroy

  has_many :regions, :through => :froms, :source => :regions
  has_many :from_regions, through: :froms, source: :region
  has_many :to_regions, through: :tos, source: :region

  accepts_nested_attributes_for :rates, allow_destroy: true

  def from_region_names
    from_regions.map {|r| r.name}.join(", ")
  end

  def from_region_codes
    from_regions.map {|r| r.code}.join(", ")
  end

  def to_region_names
    to_regions.map {|r| r.name}.join(", ")
  end

  def to_region_codes
    to_regions.map {|r| r.code}.join(", ")
  end
end
