class FescoInt < ActiveRecord::Base
  belongs_to :line
  has_many :fesco_int_tariffs

  accepts_nested_attributes_for :fesco_int_tariffs, allow_destroy: true

  def self.index_tariffs
    self.find_each do |fi|
      fi.pols.each do |pol|
        $redis.sadd "FESCO:POLS", pol
        fi.pods.each do |pod|
          $redis.sadd "FESCO:PODS", pod
          fi.fesco_int_tariffs.each do |fit|
            fit.containers.each do |container|          
              $redis.sadd "FESCO:containers", container
              $redis.sadd "FESCO:status", fit.status
              $redis.sadd "FESCO:owners", fit.owner
              $redis.hmset "FESCO:#{pol}:#{pod}:#{container}:#{fit.status}:#{fit.owner}",
                           'company',   'FESCO',
                           'pol',       pol,
                           'pod',       pod,
                           'container', container,
                           'status',    fit.status,
                           'owners',    fit.owner,
                           'freight',   fit.freight,
                           'baf',       fit.baf
            end
          end
        end
      end
    end
  end

end
