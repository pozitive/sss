class Company < ActiveRecord::Base
  has_many :lines
  validates_presence_of :name, :code

end
