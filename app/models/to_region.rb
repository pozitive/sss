class ToRegion < ActiveRecord::Base
  belongs_to :maersk_tariff
  belongs_to :region

  validates_uniqueness_of :maersk_tariff_id, scope: [:region_id]
end
