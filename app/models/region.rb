class Region < ActiveRecord::Base
  has_many :froms, :class_name => "FromRegion"
  has_many :tos, :class_name => "ToRegion"

  has_many :from_maersk_tariffs, 
           through: :from_regions,
           source: :maersk_tariff

  has_many :to_maersk_tariffs, 
           through: :to_regions,
           source: :maersk_tariff

  validates :code, uniqueness: { case_sensitive: false }
end
