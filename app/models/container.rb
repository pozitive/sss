class Container < ActiveRecord::Base
  validates :code, uniqueness: { case_sensitive: false }
end
