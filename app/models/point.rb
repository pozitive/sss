class Point < ActiveRecord::Base
  belongs_to :line

  def previous
    line.points.find_by(number: (number-1))
  end
  
  def next
    line.points.find_by(number: (number+1))
  end
end
