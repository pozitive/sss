class FescoIntTariff < ActiveRecord::Base
  belongs_to :fesco_int

  def self.get_all_redis(
        pols = fesco_pols,
        pods = fesco_pods,
        containers = fesco_containers,
        status = fesco_status,
        owners = fesco_owners
      )
    pols.map do |l|
      pods.map do |d|
        containers.map do |c|
          status.map do |st|
            owners.map do |ow|
              $redis.hgetall "FESCO:#{l}:#{d}:#{c}:#{st}:#{ow}"
            end
          end
        end
      end
    end.flatten
  end

  private

  def self.fesco_pols
    $redis.smembers "FESCO:POLS"
  end

  def self.fesco_pods
    $redis.smembers "FESCO:PODS"
  end

  def self.fesco_containers
    $redis.smembers "FESCO:containers"
  end

  def self.fesco_status
    $redis.smembers "FESCO:status"
  end

  def self.fesco_owners
    $redis.smembers "FESCO:owners"
  end
end
