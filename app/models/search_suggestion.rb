class SearchSuggestion
  def self.terms_for(prefix)
    if prefix.nil? || prefix.empty?
    then
      []
    else
      $redis.zrevrange "search-suggestions:#{prefix.downcase}", 0, -1
    end
  end 

  def self.index_seaport
    Seaport.find_each do |seaport|
      index_term(seaport.code_name_country, seaport.code)
      index_term(seaport.name, seaport.code)
      index_term(seaport.code, seaport.code)
      index_term(seaport.country, seaport.code)
    end
  end

  def self.index_term(term, code)
    1.upto(term.length) do |n|
      prefix = term[0, n]
      $redis.zincrby "search-suggestions:#{prefix.downcase}", 1, code
    end
  end

  def self.fill_redis_seaports
    Seaport.find_each do |s|
      $redis.lpush "seaports:codes", s.code
      $redis.hmset "seaports:#{s.code}",
                   "code",      s.code,
                   "name",      s.name,
                   "country",   s.country,
                   "utc",       s.utc,
                   "phone",     s.phone,
                   "web",       s.web,
                   "content",   s.content,
                   "latitude",  s.latitude,
                   "longitude", s.longitude
    end
  end
end

