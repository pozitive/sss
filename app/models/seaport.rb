class Seaport < ActiveRecord::Base
  extend FriendlyId
  validates_presence_of :name, :country, :code
  validates :code, uniqueness: { case_sensitive: false }

  friendly_id :code
  
  def code_name_country
    "#{code}, #{name}, #{country}"
  end
end
