class Line < ActiveRecord::Base
  belongs_to :company
  belongs_to :maersk_tariff

  has_many :fesco_ints
  has_many :points, -> { order(:number) }
  has_many :seaports, :through => :points

  validates_presence_of :name, :code
  validates_uniqueness_of :code, scope: [:company_id]

  def generate_number
    return 1 if points.last.nil?
    points.last.number + 1
  end

  def code_name
    "#{code} #{name}"
  end
end
