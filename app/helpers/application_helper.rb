module ApplicationHelper
  def markdown(text)
    text ||=''
    options = {
      hard_wrap: true,
      space_after_headers: true,
      fenced_code_blocks: true
    }
    
    extensions = {
      autolink: true,
      superscript: true,
      disable_indented_code_blocks: true
    }
    
    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)
    
    markdown.render(text).html_safe
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end
end
