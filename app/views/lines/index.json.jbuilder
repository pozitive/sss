json.array!(@lines) do |line|
  json.extract! line, :id, :name, :code
  json.url line_url(line, format: :json)
end
