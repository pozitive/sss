json.array!(@rates) do |rate|
  json.extract! rate, :id, :maersk_tariff_id, :container_id, :sbf
  json.url rate_url(rate, format: :json)
end
