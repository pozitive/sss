json.array!(@regions) do |region|
  json.extract! region, :id, :name, :code, :description
  json.url region_url(region, format: :json)
end
