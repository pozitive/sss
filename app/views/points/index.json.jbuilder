json.array!(@points) do |point|
  json.extract! point, :id, :number, :seaport_id, :eta, :ets, :discharge, :load, :transit_time, :line_id
  json.url point_url(point, format: :json)
end
