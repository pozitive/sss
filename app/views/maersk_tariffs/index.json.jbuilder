json.array!(@maersk_tariffs) do |maersk_tariff|
  json.extract! maersk_tariff, :id, :currency
  json.url maersk_tariff_url(maersk_tariff, format: :json)
end
