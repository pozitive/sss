json.array!(@networks) do |network|
  json.extract! network, :id, :name, :code, :description
  json.url network_url(network, format: :json)
end
