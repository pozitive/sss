json.seaports(@seaports) do |seaport|
  json.extract! seaport, :code, :name, :country, :longitude, :latitude, :utc, :phone, :web
  json.url seaport_url(seaport, format: :json)
end
