json.array!(@fesco_int_rates) do |fesco_int_rate|
  json.extract! fesco_int_rate, :id, :status, :owner, :freight, :baf
  json.url fesco_int_rate_url(fesco_int_rate, format: :json)
end
