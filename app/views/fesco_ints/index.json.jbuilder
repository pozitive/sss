json.array!(@fesco_ints) do |fesco_int|
  json.extract! fesco_int, :id, :line_id, :pols, :pods, :status, :owner, :containers, :freight, :baf
  json.url fesco_int_url(fesco_int, format: :json)
end
