json.array!(@fesco_int_tariffs) do |fesco_int_tariff|
  json.extract! fesco_int_tariff, :id, :fesco_int_id, :status, :owner, :containers, :freight, :baf
  json.url fesco_int_tariff_url(fesco_int_tariff, format: :json)
end
