json.array!(@companies) do |company|
  json.extract! company, :id, :name, :code, :web, :content, :logo
  json.url company_url(company, format: :json)
end
